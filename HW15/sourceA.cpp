#include "sqlite3.h"
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>

using namespace std;

unordered_map<string, vector<string>> results;

int getLastId(void* notUsed, int argc, char** argv, char** azCol);
int callback(void* notUsed, int argc, char** argv, char** azCol);
void printTable();
void clearTable();

int main(int argc, char* argv[])
{
	int fp;
	sqlite3* db;
	char* zErrMsg = 0;
	char* sql;

	fp = sqlite3_open("FirstPart.db", &db);

	if (fp)
	{
		cout << "Can't open database: " << sqlite3_errmsg(db) << endl;
		system("PAUSE");
		return(0);
	}
	else
		cout << "Opened database successfully" << endl;

	sql = "CREATE TABLE PEOPLE(" \
		"ID INTEGER PRIMARY KEY AUTOINCREMENT," \
		"NAME TEXT);" \
		"INSERT INTO PEOPLE(NAME)"  \
		"VALUES('Yogev'); " \
		"INSERT INTO PEOPLE(NAME)"  \
		"VALUES('Yoni'); " \
		"INSERT INTO PEOPLE(NAME)"  \
		"VALUES('Yoraz'); " \
		"UPDATE PEOPLE SET NAME = 'avichi'"  \
		"WHERE NAME = 'Yoraz'; ";
	fp = sqlite3_exec(db, sql, callback, 0, &zErrMsg);

	if (fp != SQLITE_OK)
	{
		cout << stderr << " SQL error:" << zErrMsg << endl;
		sqlite3_free(zErrMsg);
	}
	else
		cout << "No error" << endl;
	//printTable();
	sqlite3_close(db);
	system("PAUSE");
}



void clearTable()
{
	for (auto it = results.begin(); it != results.end(); ++it)
	{
		it->second.clear();
	}
	results.clear();
}

void printTable()
{
	auto iter = results.end();
	iter--;
	//	int size = results.begin()->second.size();
	int size = iter->second.size();
	for (int i = -2; i < size; i++)
	{
		for (auto it = results.begin(); it != results.end(); ++it)
		{
			if (i == -2)
			{
				//cout << it->first << " ";
				printf("|%*s|", 13, it->first.c_str());
			}
			else if (i == -1)
				cout << "_______________";
			else
				//	cout << it->second.at(i) << " ";
				printf("|%*s|", 13, it->second.at(i).c_str());
		}
		cout << endl;
	}
}

int callback(void* notUsed, int argc, char** argv, char** azCol)
{
	int i;

	for (i = 0; i < argc; i++)
	{
		auto it = results.find(azCol[i]);
		if (it != results.end())
		{
			it->second.push_back(argv[i]);
		}
		else
		{
			pair<string, vector<string>> p;
			p.first = azCol[i];
			p.second.push_back(argv[i]);
			results.insert(p);
		}
	}

	return 0;
}

int getLastId(void* notUsed, int argc, char** argv, char** azCol)
{
	cout << "the Last id is: " << argv[0] << endl;
	return 0;
}
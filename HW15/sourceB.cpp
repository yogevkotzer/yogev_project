#include "sqlite3.h"
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>


using namespace std;

unordered_map<string, vector<string>> results;

int getLastId(void* notUsed, int argc, char** argv, char** azCol);
int callback(void* notUsed, int argc, char** argv, char** azCol);
bool carPurchase(int buyerid, int carid, sqlite3* db, char* zErrMsg);
bool balanceTransfer(int from, int to, int amount, sqlite3* db, char* zErrMsg);
void whoCanBuy(int carId, sqlite3* db, char* zErrMsg);
void printTable();
void clearTable();

int main(int argc, char* argv[])
{
	int fp;
	sqlite3* db;
	char* zErrMsg = 0;
	char* sql;

	fp = sqlite3_open("carsDealer.db", &db);

	if (fp)
	{
		cout << "Can't open database: " << sqlite3_errmsg(db) << endl;
		system("PAUSE");
		return(0);
	}
	else
		cout << "Opened database successfully" << endl;
	/*
	sql = "SELECT * FROM accounts;"; \
		"ID INTEGER PRIMARY KEY AUTOINCREMENT," \
		"NAME TEXT);";
	fp = sqlite3_exec(db, sql, callback, 0, &zErrMsg);

	if (fp != SQLITE_OK)
	{
		cout << stderr << " SQL error:" << zErrMsg << endl;
		sqlite3_free(zErrMsg);
	}
	else
		cout << "" << endl;*/
	//printTable();
	whoCanBuy(23, db, zErrMsg); // who can buy car 23
	clearTable();
	carPurchase(1, 1, db, zErrMsg); // can expensive
	clearTable();
	carPurchase(1, 23, db, zErrMsg); // good buy
	clearTable();
	carPurchase(1, 18, db, zErrMsg); // good buy
	clearTable();
	balanceTransfer(2, 3, 42, db, zErrMsg); // transfer
	clearTable();
	sqlite3_close(db);
	system("PAUSE");
}

/*
prints id of all people who can buy a car
INPUT: car id, database, errorMsg holder
OUTPUT: NONE
*/
void whoCanBuy(int carId, sqlite3* db, char* zErrMsg) {
	int fp, price;
	fp = sqlite3_exec(db, (char*)((string)("SELECT balance FROM accounts;")).c_str(), callback, 0, &zErrMsg);

	if (fp != SQLITE_OK)
	{
		cout << stderr << " SQL error:" << zErrMsg << endl;
		sqlite3_free(zErrMsg);
	}
	else
		cout << "" << endl;

	fp = sqlite3_exec(db, (char*)((string)("SELECT price FROM cars WHERE id = " + to_string(carId) + ";")).c_str(), callback, 0, &zErrMsg);

	if (fp != SQLITE_OK)
	{
		cout << stderr << " SQL error:" << zErrMsg << endl;
		sqlite3_free(zErrMsg);
	}
	else
		cout << "" << endl;
	
	price = stoi(results["price"][results["price"].size() - 1]);

	for (int i = 1; i <= results["balance"].size(); i++)
		if (stoi(results["balance"][i - 1]) >= price)
			cout << i << " can buy the car" << endl;
}

/*
This function buys a car
INPUT: buyer id, car id, database, errormsg holder
OUTPUT: was sucessfull purchase
*/
bool carPurchase(int buyerid, int carid, sqlite3* db, char* zErrMsg) {
	char *sql, *temp;
	int fp, bal, cost;

	fp = sqlite3_exec(db, (char*)((string)"SELECT available FROM cars WHERE id = " + to_string(carid) + ";").c_str(), callback, 0, &zErrMsg);

	if (fp != SQLITE_OK)
	{
		cout << stderr << " SQL error:" << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		return false;
	}
	else
		cout << "" << endl;

	if (stoi(results["available"][results["available"].size() - 1]) == 2) {
		cout << "car is taken" << endl;
		return false;
	}
	fp = sqlite3_exec(db, (char*)((string)"SELECT balance FROM accounts WHERE id = " + to_string(buyerid) + ";").c_str(), callback, 0, &zErrMsg);

	if (fp != SQLITE_OK)
	{
		cout << stderr << " SQL error:" << zErrMsg << endl;
		sqlite3_free(zErrMsg);
	}
	else
		cout << "" << endl;

	fp = sqlite3_exec(db, (char*)((string)"SELECT price FROM cars WHERE id = " + to_string(carid) + ";").c_str(), callback, 0, &zErrMsg);

	if (fp != SQLITE_OK)
	{
		cout << stderr << " SQL error:" << zErrMsg << endl;
		sqlite3_free(zErrMsg);
	}
	else
		cout << "" << endl;

	if (stoi(results["balance"][results["balance"].size() - 1]) - stoi(results["price"][results["price"].size() - 1]) >= 0) {
		fp = sqlite3_exec(db, (char*)((string)("UPDATE accounts SET balance = " + to_string(stoi(results["balance"][results["balance"].size() - 1]) - stoi(results["price"][results["price"].size() - 1])) + " WHERE id = " + to_string(buyerid) + ";")).c_str(), callback, 0, &zErrMsg);

		if (fp != SQLITE_OK)
		{
			cout << stderr << " SQL error:" << zErrMsg << endl;
			sqlite3_free(zErrMsg);
		}
		else
			cout << "" << endl;

		fp = sqlite3_exec(db, (char*)((string)"UPDATE cars set available = 2 WHERE id = " + to_string(carid) + ";").c_str(), callback, 0, &zErrMsg);

		if (fp != SQLITE_OK)
		{
			cout << stderr << " SQL error:" << zErrMsg << endl;
			sqlite3_free(zErrMsg);
		}
		else
			cout << "" << endl;
		return true;
		}
	else
		cout << "Couldn't finish purchase, not enough $$$$" << endl;
	return false;
}

void clearTable()
{
	for (auto it = results.begin(); it != results.end(); ++it)
	{
		it->second.clear();
	}
	results.clear();
}

void printTable()
{
	auto iter = results.end();
	iter--;
	//	int size = results.begin()->second.size();
	int size = iter->second.size();
	for (int i = -2; i < size; i++)
	{
		for (auto it = results.begin(); it != results.end(); ++it)
		{
			if (i == -2)
			{
				//cout << it->first << " ";
				printf("|%*s|", 13, it->first.c_str());
			}
			else if (i == -1)
				cout << "_______________";
			else
				//	cout << it->second.at(i) << " ";
				printf("|%*s|", 13, it->second.at(i).c_str());
		}
		cout << endl;
	}
}

int callback(void* notUsed, int argc, char** argv, char** azCol)
{
	int i;

	for (i = 0; i < argc; i++)
	{
		auto it = results.find(azCol[i]);
		if (it != results.end())
		{
			it->second.push_back(argv[i]);
		}
		else
		{
			pair<string, vector<string>> p;
			p.first = azCol[i];
			p.second.push_back(argv[i]);
			results.insert(p);
		}
	}

	return 0;
}

int getLastId(void* notUsed, int argc, char** argv, char** azCol)
{
	cout << "the Last id is: " << argv[0] << endl;
	return 0;
}


/*
This function transfers money
INPUT: id from, id to, money amount, database, errorMsg holder
OUTPUT: was sucessfull
*/
bool balanceTransfer(int from, int to, int amount, sqlite3* db, char* zErrMsg) {
	int fp, bal1, bal2;
	fp = sqlite3_exec(db, (char*)((string)("SELECT balance FROM accounts WHERE id =" + to_string(to) + ";")).c_str(), callback, 0, &zErrMsg);

	if (fp != SQLITE_OK)
	{
		cout << stderr << " SQL error:" << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		return false;
	}
	else
		cout << "" << endl;

	bal1 = stoi(results["balance"][results["balance"].size() - 1]);

	fp = sqlite3_exec(db, (char*)((string)("SELECT balance FROM accounts WHERE id =" + to_string(from) + ";")).c_str(), callback, 0, &zErrMsg);

	if (fp != SQLITE_OK)
	{
		cout << stderr << " SQL error:" << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		return false;
	}
	else
		cout << "" << endl;

	bal2 = stoi(results["balance"][results["balance"].size() - 1]);

	if (bal2 - bal1 < 0)
		return false;

	
	fp = sqlite3_exec(db, (char*)((string)("UPDATE accounts SET balance = " + to_string(stoi(results["balance"][results["balance"].size() - 1]) - amount) + " WHERE id = " + to_string(from) + ";")).c_str(), callback, 0, &zErrMsg);

	if (fp != SQLITE_OK)
	{
		cout << stderr << " SQL error:" << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		return false;
	}
	else
		cout << "" << endl;

	

	fp = sqlite3_exec(db, (char*)((string)("UPDATE accounts SET balance = " + to_string(stoi(results["balance"][results["balance"].size() - 2]) + amount) + " WHERE id = " + to_string(to) + ";")).c_str(), callback, 0, &zErrMsg);

	if (fp != SQLITE_OK)
	{
		cout << stderr << " SQL error:" << zErrMsg << endl;
		sqlite3_free(zErrMsg);
	}
	else
		cout << "" << endl;
	return true;
}
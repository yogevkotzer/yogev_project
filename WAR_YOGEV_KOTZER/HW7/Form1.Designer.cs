﻿namespace HW7
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Forfeit = new System.Windows.Forms.Button();
            this.meScore = new System.Windows.Forms.Label();
            this.enScore = new System.Windows.Forms.Label();
            this.enemyCard = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.enemyCard)).BeginInit();
            this.SuspendLayout();
            // 
            // Forfeit
            // 
            this.Forfeit.Location = new System.Drawing.Point(322, 12);
            this.Forfeit.Name = "Forfeit";
            this.Forfeit.Size = new System.Drawing.Size(115, 43);
            this.Forfeit.TabIndex = 11;
            this.Forfeit.Text = "Chicken out";
            this.Forfeit.UseVisualStyleBackColor = true;
            this.Forfeit.Click += new System.EventHandler(this.Forfeit_Click);
            // 
            // meScore
            // 
            this.meScore.AutoSize = true;
            this.meScore.Font = new System.Drawing.Font("Miriam Fixed", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.meScore.Location = new System.Drawing.Point(12, 46);
            this.meScore.Name = "meScore";
            this.meScore.Size = new System.Drawing.Size(246, 28);
            this.meScore.TabIndex = 12;
            this.meScore.Text = "Your Score: 0";
            this.meScore.Click += new System.EventHandler(this.meScore_Click);
            // 
            // enScore
            // 
            this.enScore.AutoSize = true;
            this.enScore.Font = new System.Drawing.Font("Miriam Fixed", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.enScore.Location = new System.Drawing.Point(460, 46);
            this.enScore.Name = "enScore";
            this.enScore.Size = new System.Drawing.Size(295, 32);
            this.enScore.TabIndex = 13;
            this.enScore.Text = "Enemy Score: 0";
            // 
            // enemyCard
            // 
            this.enemyCard.Image = global::HW7.Properties.Resources.card_back_blue;
            this.enemyCard.Location = new System.Drawing.Point(322, 61);
            this.enemyCard.Name = "enemyCard";
            this.enemyCard.Size = new System.Drawing.Size(115, 104);
            this.enemyCard.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.enemyCard.TabIndex = 14;
            this.enemyCard.TabStop = false;
            this.enemyCard.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Lime;
            this.ClientSize = new System.Drawing.Size(759, 297);
            this.Controls.Add(this.enemyCard);
            this.Controls.Add(this.enScore);
            this.Controls.Add(this.meScore);
            this.Controls.Add(this.Forfeit);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.enemyCard)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button Forfeit;
        private System.Windows.Forms.Label meScore;
        private System.Windows.Forms.Label enScore;
        private System.Windows.Forms.PictureBox enemyCard;
    }
}


﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace HW7
{
    public partial class Form1 : Form
    {
        PictureBox[] myCards = new PictureBox[10];
        public int power = 0;
        public int character = 0;
        public bool cardClicked = false;
        public NetworkStream clientConnect;
        public bool color = true; // pick your color
        public Form1()
        {
            InitializeComponent();
            //ChangePhoto(true);
        }



        // False Red, True blue
        public void GenCards()
        {
            enemyCard.Image = !color ? Image.FromFile("card back blue.png") : Image.FromFile("card back red.png"); // enemy card

            for (int i = 0; i < 10; i++){
                PictureBox temp = new PictureBox
                {
                    Name = "myCard" + i, // card name
                    Size = new Size(this.Width / 12, this.Height / 3), // set size of card
                    Enabled = true,
                    SizeMode = PictureBoxSizeMode.StretchImage, 
                    Image = color ? Image.FromFile("card back blue.png") : Image.FromFile("card back red.png"), // card loyalty (red or blue)
                    Location = new Point((this.Width / 10) * i, this.Height - this.Height / 3) // set spot
                };
                myCards[i] = temp; // temp card holder
                temp.Click += delegate (object sender1, EventArgs e1) {
                    enemyCard.Image = !color ? Image.FromFile("card back blue.png") : Image.FromFile("card back red.png"); // reset enemy cards
                    resetCards(); // reset ally cards
                    Random randomPower = new Random();
                    power = randomPower.Next(1, 14);
                    character = randomPower.Next(1, 5);
                    updateCard(temp);
                    cardClicked = true; // stop the while
                };
                this.Controls.Add(temp); // add to controls
            }
           
            //SystemInformation.BorderSize;
        }
        
        private void btnForfeit_Click(object sender, EventArgs e)
        {
            endGame();
            this.Dispose();
            byte[] buffer = new ASCIIEncoding().GetBytes("2000");
            clientConnect.Write(buffer, 0, buffer.Length);
            clientConnect.Flush();
        }

        /*
         * This function gets a card and changes it
         * INPUT: card picturebox
         */
        private void updateCard(PictureBox toChange)
        {

            // using global variables
            string powerComp = power.ToString();
            if (power > 10 || power == 1)
            {
                switch (powerComp) {
                    case "11":
                        powerComp = "jack";
                        break;
                    case "12":
                        powerComp = "queen";
                        break;
                    case "13":
                        powerComp = "king";
                        break;
                    case "1":
                        powerComp = "ace";
                        break;
                }
            }
            string characterComp = character.ToString();
            switch (character)
            {
                case 1:
                    characterComp = "_of_clubs";
                    break;
                case 2:
                    characterComp = "_of_diamonds";
                    break;
                case 3:
                    characterComp = "_of_hearts";
                    break;
                case 4:
                    characterComp = "_of_spades";
                    break;
            }

            //adding 2 at the end at some of the card names
            if ((power == 1 && character == 4) || (power < 14 && power > 10)) characterComp += "2";
            Console.WriteLine(powerComp + characterComp);
            toChange.Image = Image.FromFile(powerComp + characterComp + ".png");
            toChange.Refresh();
        }

//         This function manages client connections
        private void clientFunc(object obj)
        {
            TcpClient client = new TcpClient();
            IPEndPoint serverEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 8820);
            try
            {
                client.Connect(serverEndPoint);
            }
            catch
            {
                Invoke((MethodInvoker)delegate {
                    MessageBox.Show("Could not connect to server.");
                    this.Dispose();
                });
                return;
            }

            clientConnect = client.GetStream();
            byte[] buffer = new byte[4];
            int bytesRead = clientConnect.Read(buffer, 0, 4);
            string input = new ASCIIEncoding().GetString(buffer);
            Invoke((MethodInvoker)delegate { GenCards(); });

            while (true)
            {
                while (!cardClicked) continue;
                cardClicked = false;
                string cardCode = power.ToString();
                if (cardCode.Length == 1) cardCode = "0" + cardCode;
                switch (character) {
                    case 1:
                        cardCode += "C";
                        break;
                    case 2:
                        cardCode += "D";
                        break;
                    case 3:
                        cardCode += "H";
                        break;
                    case 4:
                        cardCode += "S";
                        break;
                }


                buffer = new ASCIIEncoding().GetBytes("1" + cardCode);
                clientConnect.Write(buffer, 0, buffer.Length);
                clientConnect.Flush();

                clientConnect.Read(buffer, 0, 4);
                input = new ASCIIEncoding().GetString(buffer);
                if (input.StartsWith("1"))
                {
                    if (int.Parse(input.Substring(1, 2)) < power)
                        Invoke((MethodInvoker)delegate { meScore.Text = meScore.Text.Remove(meScore.Text.Length - 1) + (int.Parse(meScore.Text[meScore.Text.Length - 1] + "") + 1).ToString(); });
                    else if (int.Parse(input.Substring(1, 2)) > power)
                        Invoke((MethodInvoker)delegate { enScore.Text = enScore.Text.Remove(enScore.Text.Length - 1) + (int.Parse(enScore.Text[enScore.Text.Length - 1] + "") + 1).ToString(); });

                    //updating the opponent's card
                    switch (input[3]) {
                        case 'C':
                            character = 1;
                            break;
                        case 'D':
                            character = 2;
                            break;
                        case 'H':
                            character = 3;
                            break;
                        case 'S':
                            character = 4;
                            break;
                    }
// C D H S

                    power = int.Parse(input.Substring(1, 2));

                    Invoke((MethodInvoker)delegate { updateCard(enemyCard); });
                }
                else if (input.StartsWith("2"))
                {
                    Invoke((MethodInvoker)delegate {
                        endGame(); this.Dispose();
                        buffer = new ASCIIEncoding().GetBytes("2000");
                        clientConnect.Write(buffer, 0, buffer.Length);
                        clientConnect.Flush();
                    });
                }
                //function shows the score of both players in a messageBox.
                

            }
        }

        // This function shows messageBox at end of card game
        private void endGame()
        {
            MessageBox.Show("Your score:" + meScore.Text + "\nOpponent's score:" + enScore.Text, "Game Score Summary");
        }



        // Chicken out button
        private void Forfeit_Click(object sender, EventArgs e)
        {
            endGame();
            this.Dispose();
            byte[] buffer = new ASCIIEncoding().GetBytes("2000");
            clientConnect.Write(buffer, 0, buffer.Length);
            clientConnect.Flush();
        }

        
        //flip all the cards to face down
        private void resetCards()
        {
            for (int i = 0; i < myCards.Length; i++)
            {
                myCards[i].Image = color ? Image.FromFile("card back blue.png") : Image.FromFile("card back red.png");
            }
        }


        private void Form1_Load(object sender, EventArgs e)
        {
            Thread myT = new Thread(clientFunc);
            myT.Start();
        }
    }
}

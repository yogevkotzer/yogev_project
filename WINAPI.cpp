#include <iostream>
#include <vector>
#include <string>
#include <windows.h>
#include "Helper.h"
#include <functional>
#include <string>
#include <iostream>
#include <fstream>  
#include <tchar.h> 
#include <stdio.h>
#include <strsafe.h>
#pragma comment(lib, "User32.lib")
#define FUNC_SIZE 5
using namespace std;

typedef void(*FunctionPointer)(string);


#define BUFSIZE MAX_PATH
void pwd(string path = "");
void cd(string path);
void create(string name);
void ls(string block = "");
void secret(string block);
void exe(string exe);
typedef int(WINAPI* DLL_FILE_THING)(void);

int main()
{
	string inp = "sweet mother of god";
	vector<string> words;
	Helper help;
	int i;
	string func_names[] = { "pwd" , "cd", "create", "ls", "secret"};
	FunctionPointer func[] = { pwd , cd , create, ls, secret};
	goto end;
	while (inp != "nope") {
		for (i = 0; i < FUNC_SIZE; i++) {
			if (words[0] == func_names[i]) {
				switch (words.size())
				{
				case 1:
					func[i]("");
					goto end;
				case 2:
					func[i](words[1]);
					goto end;
				default:
					cout << "error" << endl;
					break;
				}
			}
		}
		
	end:
		getline(cin, inp);
		//cin >> inp;
		words = help.get_words(inp);
	}
	return 0;
}

void pwd(string path) { 
	TCHAR Buffer[BUFSIZE];
	DWORD dwRet;
	dwRet = GetCurrentDirectory(BUFSIZE, Buffer);
	cout << Buffer << endl;
}

void cd(string path) {	if(!SetCurrentDirectory(path.c_str())) cout << "shit" << endl; }

void create(string name) {
	std::ofstream outfile(name);
	outfile.close();
}


void ls(string block) {
	TCHAR Buffer[BUFSIZE];
	DWORD dwRet;
	dwRet = GetCurrentDirectory(BUFSIZE, Buffer);
	strcat_s(Buffer, "\\*");
	WIN32_FIND_DATA data;
	HANDLE hFind = FindFirstFile(Buffer, &data);  // FILES

	if (hFind != INVALID_HANDLE_VALUE) {
		do {
			std::cout << data.cFileName << std::endl;
		} while (FindNextFile(hFind, &data));
		FindClose(hFind);
	}
}

void secret(string block)
{
	HMODULE dll = LoadLibraryA("SECRET.dll"); //load dll file
	if (dll != NULL)
	{
		DLL_FILE_THING func = (DLL_FILE_THING)GetProcAddress(dll, "TheAnswerToLifeTheUniverseAndEverything"); //get the function, could have been a shorter name because I spelled it wrong, DAMMIT MAGSHIMIM!
		if (func != NULL)
			cout << "TheAnswerToLifeTheUniverseAndEverything:" << func() << endl; // use function
	}
}

void exe(string exe)
{
	LPSTR command = const_cast<char *>(exe.c_str()); // path to LPSTR

	STARTUPINFO startInfo;
	PROCESS_INFORMATION processInfo;

	ZeroMemory(&startInfo, sizeof(startInfo));
	startInfo.cb = sizeof(startInfo);
	ZeroMemory(&processInfo, sizeof(processInfo));

	// Start the child process. 
	CreateProcessA(NULL, command, NULL, NULL, FALSE, 0, NULL, NULL,&startInfo, &processInfo);
	WaitForSingleObject(processInfo.hProcess, INFINITE); //wait until child ends
	DWORD exitCode = 0;
	GetExitCodeProcess(processInfo.hProcess, &exitCode); // to know what the exit code - if there where errors etc.
	printf("exit code is %X\n", exitCode);

	// Kill process in cold blood
	CloseHandle(processInfo.hProcess);
	CloseHandle(processInfo.hThread);
}